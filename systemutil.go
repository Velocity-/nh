package main

import (
	"os"
	"io/ioutil"
	"os/exec"
)

func HasFile(file string) bool {
	_, e := os.Stat(file)
	return e == nil
}

func GetFileBytes(file string) ([]byte, error) {
	return ioutil.ReadFile(file)
}

func GetFileUTF8(file string) (string, error) {
	b, err := GetFileBytes(file)
	if err != nil {
		return "", err
	}

	return string(b), nil
}

func GetStdoutBytes(combined bool, cmd string, arg ...string) ([]byte, error) {
	command := exec.Command(cmd, arg...)

	if combined {
		return command.CombinedOutput()
	} else {
		return command.Output()
	}
}

func GetStdoutStr(combined bool, cmd string, arg ...string) (string, error) {
	bytes, err := GetStdoutBytes(combined, cmd, arg...)
	return string(bytes), err
}