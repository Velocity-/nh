package main

import (
	"bufio"
	"encoding/binary"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"os"
	"os/exec"
	"runtime"
	"strings"
	"time"
	"gopkg.in/ini.v1"
	"github.com/shirou/gopsutil/host"
	"github.com/satori/go.uuid"
	"crypto/sha256"
	"github.com/shirou/gopsutil/mem"
	"github.com/shirou/gopsutil/disk"
)

func MakePacket(pType int, bufLen int) *PacketBuffer {
	databuf := make([]byte, bufLen)
	buffer := &PacketBuffer{bytes: databuf, position: 0, packetType: pType}
	return buffer
}

type AuthStatus int

const (
	Disconnected   = 1
	Connected      = 2
	Authenticating = 3
	Authenticated  = 4
)

const (
	Exec_PipeData  = 1
	Exec_NoTimeout = 2
	Exec_NoShell   = 4
)

var ENDPOINT = "localhost:12300"
var TOKEN = "f10e2821bbbea527ea02200352313bc059445190"
var Config *NodeConfig
var status AuthStatus = Disconnected
var conn net.Conn

const VERSION = "0.1.0"

func main() {
	args := os.Args[1:]
	ENDPOINT = args[0]
	TOKEN = args[1]

	Config = processConfigOrSetup()

	var bufRd *bufio.Reader

	// Start delay-based logic
	go handlePeriodicEvents()

	// Begin reading
	header := make([]byte, 6)

	for {
		if status == Disconnected {
			myconn, err := net.DialTimeout("tcp", ENDPOINT, 10 * time.Second)
			if err != nil {
				// handle error
				log.Println("Error connecting. Trying again...")
				time.Sleep(1 * time.Second)
				continue
			}

			conn = myconn
			bufRd = bufio.NewReader(conn)

			status = Connected
		} else if status == Connected {
			// Authenticate real quick
			pktAuth := MakePacket(1, 512)
			pktAuth.WriteUTF8(VERSION)
			pktAuth.WriteUTF8(TOKEN)
			pktAuth.WriteUTF8(Config.Identifier)

			hostInfo, hErr := host.Info()
			if hErr == nil {
				pktAuth.WriteUTF8(hostInfo.Hostname)
				pktAuth.WriteUTF8(hostInfo.OS)
				pktAuth.WriteUTF8(runtime.GOARCH)
				pktAuth.WriteUTF8(hostInfo.Platform)
				pktAuth.WriteUTF8(hostInfo.PlatformFamily)
				pktAuth.WriteUTF8(hostInfo.PlatformVersion)
				pktAuth.WriteUTF8(hostInfo.KernelVersion)
				pktAuth.WriteUTF8(hostInfo.HostID)
			} else {
				pktAuth.WriteUTF8("unknown.host.name")
				pktAuth.WriteUTF8(runtime.GOOS)
				pktAuth.WriteUTF8(runtime.GOARCH)
				pktAuth.WriteUTF8("")
				pktAuth.WriteUTF8("")
				pktAuth.WriteUTF8("")
				pktAuth.WriteUTF8("")
				pktAuth.WriteUTF8("")
			}

			WritePacket(conn, pktAuth)
			status = Authenticating
		} else if status == Authenticating || status == Authenticated {
			// Read from the last marker pos
			_, err := bufRd.Peek(6)
			if err != nil {
				status = Disconnected
				continue
			}

			_, err = bufRd.Read(header)
			if err != nil {
				status = Disconnected
				continue
			}

			packetType := binary.BigEndian.Uint16(header)
			packetSize := binary.BigEndian.Uint32(header[2:])

			log.Println("Packet id", packetType, "and size", packetSize)

			// Allocate data
			data := make([]byte, packetSize)
			ptr := uint32(0)

			for ptr < packetSize {
				add, err := bufRd.Read(data[ptr:])

				if err != nil {
					status = Disconnected
					continue
				}

				ptr += uint32(add)
			}

			log.Println("Got a total of", ptr, "bytes")
			status = handleMessage(int(packetType), data, conn)
		}
	}
}

func handlePeriodicEvents() {
	memoryUpdate := time.NewTicker(5 * time.Second)

	for {
		select {
		case <- memoryUpdate.C: // Refresh memory to server
			// Add memory stats..
			memStats, _ := mem.VirtualMemory()
			updateMem := MakePacket(3, 64)
			updateMem.WriteUint64(memStats.Total)
			updateMem.WriteUint64(memStats.Used)

			// Add disk stats...
			// TODO Support windows here
			diskStats, _ := disk.Usage("/")
			updateMem.WriteUTF8(diskStats.Path)
			updateMem.WriteUint64(diskStats.Total)
			updateMem.WriteUint64(diskStats.Used)

			WritePacket(conn, updateMem)
		}
	}
}

func findMainPartition() string {
	partitions, _ := disk.Partitions(false)

	for _, v := range partitions {
		fmt.Printf("Part: %+v\n", v)
		if !strings.Contains(v.Opts, "ro") {
			return v.Mountpoint
		}
	}

	return ""
}

type PacketBuffer struct {
	bytes      []byte
	position   int
	packetType int
}

func (b *PacketBuffer) WriteByte(v byte) {
	b.bytes[b.position] = v
	b.position++
}

func (b *PacketBuffer) WriteUint16(v uint16) {
	b.bytes[b.position] = byte(v >> 8)
	b.bytes[b.position+1] = byte(v)
	b.position += 2
}

func (b *PacketBuffer) WriteUint32(v uint32) {
	b.bytes[b.position] = byte(v >> 24)
	b.bytes[b.position+1] = byte(v >> 16)
	b.bytes[b.position+2] = byte(v >> 8)
	b.bytes[b.position+3] = byte(v)
	b.position += 4
}

func (b *PacketBuffer) WriteUint64(v uint64) {
	b.bytes[b.position] = byte(v >> 56)
	b.bytes[b.position+1] = byte(v >> 48)
	b.bytes[b.position+2] = byte(v >> 40)
	b.bytes[b.position+3] = byte(v >> 32)
	b.bytes[b.position+4] = byte(v >> 24)
	b.bytes[b.position+5] = byte(v >> 16)
	b.bytes[b.position+6] = byte(v >> 8)
	b.bytes[b.position+7] = byte(v)
	b.position += 8
}

func (b *PacketBuffer) ReadUint8() uint8 {
	v := b.bytes[b.position]
	b.position++
	return uint8(v)
}

func (b *PacketBuffer) ReadUint16() uint16 {
	v := binary.BigEndian.Uint16(b.bytes[b.position:])
	b.position += 2
	return v
}

func (b *PacketBuffer) WriteUTF8(str string) {
	stringBytes := []byte(str)
	b.WriteUint16(uint16(len(stringBytes)))
	copy(b.bytes[b.position:], stringBytes)
	b.position += len(stringBytes)
}

func (b *PacketBuffer) ReadUTF8() string {
	strlen := int(b.ReadUint16())
	strData := b.bytes[b.position : b.position+strlen]
	b.position += strlen
	return string(strData)
}

func WritePacket(conn net.Conn, c *PacketBuffer) {
	if status == Disconnected {
		return
	}

	header := make([]byte, 6)

	header[0] = byte(c.packetType >> 8)
	header[1] = byte(c.packetType)

	header[2] = byte(c.position >> 24)
	header[3] = byte(c.position >> 16)
	header[4] = byte(c.position >> 8)
	header[5] = byte(c.position)

	conn.Write(header)

	if c.position > 0 {
		conn.Write(c.bytes[0:c.position])
	}
}

func handleMessage(id int, data []byte, con net.Conn) AuthStatus {
	packet := PacketBuffer{bytes: data, position: 0}

	if id == 0x01 {
		return Authenticated
	}

	if id == 0x02 {
		options := packet.ReadUint8()
		str := packet.ReadUTF8()

		log.Println("Execute:", str, options)

		go func() {
			var cmd *exec.Cmd

			// On Windows we execute cmd /c xyz, on linux/darwin we execute bash -c xyz.
			if options&Exec_NoShell == 0 {
				if runtime.GOOS == "windows" {
					cmd = exec.Command("cmd", "/C", str)
				} else {
					cmd = exec.Command("bash", "-c", str)
				}
			} else {
				// If the NoShell option is supplied, we use the command as a filename plus args.
				// This is used in cases where bash/cmd are not available and thus need a manual command.
				split := strings.Split(str, " ")
				cmd = exec.Command(split[0], split[1:]...)
			}

			cmd.Start()
			go cmd.Wait() // Run async

			// If we don't have the NoTimeout flag set, kill after 5s.
			if options&Exec_NoTimeout == 0 {
				time.Sleep(time.Second * 5)

				// Check if process is still running before killing it off.
				if cmd.ProcessState == nil || !cmd.ProcessState.Exited() {
					cmd.Process.Kill()
				}
			}
		}()
	}

	if id == 0x03 {
		url := packet.ReadUTF8()
		go updateSelf(url)
	}

	return Authenticated
}

func updateSelf(url string) {
	selfExec := os.Args[0]
	fileName := selfExec[strings.LastIndex(selfExec, "/")+1:]
	println("Updating self from " + fileName)

	file, err := os.OpenFile(fileName, os.O_RDWR, 0)
	if err != nil {
		fmt.Println("Error while updating", err)
		return
	}

	response, err := http.Get(url)
	if err != nil {
		fmt.Println("Error while updating", url, "-", err)
		return
	}

	defer response.Body.Close()
	defer file.Close()

	n, err := io.Copy(file, response.Body)
	if err != nil {
		fmt.Println("Error while updating", url, "-", err)
		return
	}

	fmt.Println(n, "bytes downloaded. Running executable...")

	exec.Command(selfExec, os.Args[1:]...)
	os.Exit(0)
}

type NodeConfig struct {
	Identifier string
}

// doInitialSetup checks if the intial setup is needed, and if so, proceeds to install the configuration.
func processConfigOrSetup() *NodeConfig {
	_, err := os.Stat("/etc/nodehawk/config.ini")
	if err != nil {
		println("No configuration found at /etc/nodehawk/config.ini - setting up..")

		// Try to open the file for writing..
		os.MkdirAll("/etc/nodehawk", 664)
		_, err := os.Create("/etc/nodehawk/config.ini")
		if err != nil {
			panic("Error opening config file '/etc/nodehawk/config.ini' for writing: " + err.Error())
		}

		// Save key to file.
		saved := ini.Empty()
		cfg := new(NodeConfig)
		cfg.Identifier = fmt.Sprintf("%032x", sha256.New().Sum(uuid.NewV4().Bytes()))
		ini.ReflectFrom(saved, cfg)
		saved.SaveTo("/etc/nodehawk/config.ini")

		return cfg
	}

	cfg := new(NodeConfig)
	err = ini.MapTo(cfg, "/etc/nodehawk/config.ini")

	if err != nil {
		panic(err)
	}

	return cfg
}