package main

import "runtime"

type OperatingSystem int
const (
	_ OperatingSystem = iota
	Linux
	Windows
	MacOS
	BSD
)

type OSDistribution int
const (
	_ OperatingSystem = iota
	Ubuntu
	CentOS
	Debian
)

type OsArchitecture int
const (
	Arch_32 OsArchitecture = 1
	Arch_64              = 2
)

type OsInfo struct {

	BaseOS OperatingSystem
	BaseOSName string
	OSVersion string
	OsArchitecture OsArchitecture
	Distro OSDistribution
	DistroName string
	DistroVersion string

}

func GatherOSInfo() OsInfo {
	info := OsInfo {}
	info.BaseOSName = runtime.GOOS

	// Determine architecture
	if runtime.GOARCH == "amd64" {
		info.OsArchitecture = Arch_64
	} else {
		info.OsArchitecture = Arch_32
	}

	if runtime.GOOS == "linux" {
		info.BaseOS = Linux
	}

	return info
}