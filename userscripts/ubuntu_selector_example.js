///
/// Example user script for NodeHawk server commanding.
///
/// This script, when ran, will update the APT repositories on all Ubuntu servers after 14.04.
///

var script = {
    name: "Ubuntu 14.04+ APT refresh",
    author: "Bart Pelle",

    script: function(context) {
        // Fetch all servers with OS 'linux' and distribution 'ubuntu' - case-insensitive.
        context.findServers("linux", "ubuntu", function(servers) {
            var affected = 0;

            // Iterate over all known servers matching linux/ubuntu..
            for (var i = 0; i < servers.length; i++) {
                var server = servers[i];

                // Parse distro version from info, take major part.
                var distroMajor = parseInt(server.distroVersion.split(".")[0]);

                // Apply function if the server has at least major version 14.
                if (distroMajor >= 14) {
                    server.exec("apt-get update");
                    affected++;
                }
            }

            // Inform user that they have been updated.
            if (affected > 0) {
                context.alert("success", affected + " servers have had their APT repository updated.");
            } else {
                context.alert("info", "No servers available that match linux/ubuntu 14.X+.")
            }
        });
    }
};

module.exports = script;