///
/// Example user script for NodeHawk server commanding.
///
/// This script, when ran, will run 'apt-get autoremove' on all servers that have move than
/// 90% of their disk space used up.
///

var script = {
    name: "Ubuntu apt autoremove",
    author: "Bart Pelle",

    script: function(context) {
        // Fetch all servers with OS 'linux' and distribution 'ubuntu' - case-insensitive.
        context.findServers("linux", "ubuntu", function(servers) {
            var affected = 0;

            // Iterate over all known servers matching linux/ubuntu..
            for (var i = 0; i < servers.length; i++) {
                var server = servers[i];

                // Execute "du -hs" to query how much disk space we have.
                server.exec("df -h", function(process) {
                    // When process terminates, process output..
                    process.onExit(function(code, stdout, stderr) {
                        if (code === 0) { // Make sure it succeeded
                            var lines = stdout.split("\n")

                            // TODO use the output to determine whether we should autoremove.
                        }
                    });
                });
            }

            // Inform user that they have been updated.
            if (affected > 0) {
                context.alert("success", affected + " servers have had their APT repository updated.");
            } else {
                context.alert("info", "No servers available that match linux/ubuntu 14.X+.")
            }
        });
    }
};

module.exports = script;